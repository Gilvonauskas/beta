<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Žinutės</title>
        <link rel="stylesheet" media="screen" type="text/css" href="/public/css/screen.css" />
    </head>
    <body>
        <div id="wrapper">
            <h1>Jūsų žinutės</h1>
            <form method="post" action="/index/post_message" id="message_form">
                <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>">
                <p <?php if(isset($this->flashData['errors']['name'])): ?> class="err" <?php endif; ?> >
                    <label for="name">Vardas *</label><br/>
                    <input id="name" type="text" name="name" value="<?php echo $this->flashData['previousValues']['name'] ?>" />
                </p>
                <p <?php if(isset($this->flashData['errors']['lastname'])): ?> class="err" <?php endif; ?>>
                    <label for="lastname">Pavardė *</label><br/>
                    <input id="lastname" type="text" name="lastname" value="<?php echo $this->flashData['previousValues']['lastname'] ?>" />
                </p>
                <p <?php if(isset($this->flashData['errors']['birthdate'])): ?> class="err" <?php endif; ?>>
                    <label for="birthdate">Gimimo data *</label><br/>
                    <input id="birthdate" type="text" name="birthdate" value="<?php echo $this->flashData['previousValues']['birthdate'] ?>" placeholder="0000-00-00" />
                </p>
                <p <?php if(isset($this->flashData['errors']['email'])): ?> class="err" <?php endif; ?>>
                    <label for="email">El.pašto adresas</label><br/>
                    <input id="email" type="text" name="email" value="<?php echo $this->flashData['previousValues']['email'] ?>" />
                </p>
                <p <?php if(isset($this->flashData['errors']['message'])): ?> class="err" <?php endif; ?>>
                    <label for="message">Jūsų žinutė *</label><br/>
                    <textarea id="message" name="message"><?php echo $this->flashData['previousValues']['message'] ?></textarea>
                </p>
                <p>
                    <span>* - privalomi laukai</span>
                    <input type="submit" value="Skelbti" id="submit" />
                    <img src="/public/img/ajax-loader.gif" alt="" style="display:none;" id="loader" />
                </p>
            </form>
            <ul class="messages-list">
                <?php if (count($this->messages)>0): ?>
                    <?php foreach ($this->messages as $message): ?>
                        <li>
                            <span class="created_at"><?php echo $message['created_at']; ?></span>
                            
                            <?php if (!empty($message['email'])): ?>
                                <div class="name_lastname_with_email">
                                    <a href="mailto:<?php echo $message['email']; ?>">
                                        <span class="name_lastname">
                                            <?php echo $message['name'].' '.$message['lastname']; ?>
                                        </span>
                                    </a>
                                    <span class="comma">, </span>
                                    <span class="age"><?php echo $message['age']; ?></span>m.<br/>
                                </div>
                            <?php else: ?>
                                <div class="name_lastname_without_email">
                                    <span class="name_lastname"><?php echo $message['name'].' '.$message['lastname']; ?></span>
                                    <span class="comma">, </span>
                                    <span class="age"><?php echo $message['age']; ?></span>m.<br/>
                                </div>
                            <?php endif; ?>
                            
                            <div class="message">
                                <?php echo $message['message']; ?>
                            </div>
                        </li>
                    <?php endforeach; ?>
                <?php else: ?>
                    <li>
                        <strong>Šiuo metu žinučių nėra. Būk pirmas!</strong>
                    </li>
                <?php endif; ?>
            </ul>
            <p id="pages">
                <?php if($this->paginator['pages_total']>1): ?>
                    <a href="<?php echo $this->paginator['previous_page_url']; ?>" title="atgal" class="prev_page">Atgal</a>
                    <?php for ($p=1; $p<=$this->paginator['pages_total']; $p++): ?>
                        <?php if($this->paginator['current_page']==$p): ?>
                            <span class="current_page"><?php echo $p; ?></span>
                        <?php else: ?>
                            <a href="?page=<?php echo $p; ?>" class="page_link"><?php echo $p; ?></a>
                        <?php endif; ?>
                    <?php endfor; ?>
                    <a href="<?php echo $this->paginator['next_page_url']; ?>" title="toliau" class="next_page">Toliau</a>
                <?php endif; ?>
            </p>
            
            <div class="message-to-copy">
                <li>
                    <span class="created_at"></span>
                    <div class="name_lastname_with_email">
                        <a href="mailto:">
                            <span class="name_lastname"></span>
                        </a>
                        <span class="comma">, </span>
                        <span class="age"></span>m.<br/>
                    </div>
                    <div class="name_lastname_without_email">
                        <span class="name_lastname"></span>
                        <span class="comma">, </span>
                        <span class="age"></span>m.<br/>
                    </div>
                    <div class="message"></div>
                </li>
            </div>
            
            <div class="pages-to-copy">
                <a href="" title="atgal" class="prev_page">Atgal</a>
                <span class="current_page"></span>
                <a href="?page=" class="page_link"></a>
                <a href="" title="toliau" class="next_page">Toliau</a>
            </div>
            
        </div>
        <script type="text/javascript" src="/public/libs/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="/public/js/custom_functions.js"></script>
    </body>
</html>
