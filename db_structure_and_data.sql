-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 23, 2018 at 04:44 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beta`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `birthdate` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `name`, `lastname`, `birthdate`, `email`, `message`, `created_at`) VALUES
(1, 'Petras', 'Petrauskas', '2001-09-03', 'petras@example.com', 'Lorem ipsum', '2018-09-03 05:32:37'),
(2, 'Jonas', 'Jonauskas', '2002-09-04', 'jonas@example.com', 'Dolor sit amet.', '2018-09-05 07:24:29'),
(3, 'Juozas', 'Juozauskas', '2003-05-27', 'juozas@example.com', 'Lorem ipsum dolor sit amet.', '2018-09-22 18:52:37'),
(4, 'John', 'Smith', '2001-05-27', 'tgilvonas@gmail.com', 'Blah blah', '2018-09-22 18:58:47'),
(5, 'Alfonsas', 'Kazlauskas', '2002-05-27', 'tgilvonas@gmail.com', 'Lorem ipsum dolor sit amet.', '2018-09-22 18:59:51'),
(6, 'Gintaras', 'Jankauskas', '2003-05-27', '', 'Lorem ipsum dolor sit amet.', '2018-09-22 19:41:58'),
(7, 'Albinas', 'Kvietkauskas', '2001-05-27', '', 'Lorem ipsum', '2018-09-22 20:05:05'),
(8, 'Jane', 'Smith', '2002-05-27', '', 'Blah blah', '2018-09-22 21:25:08'),
(9, 'Petras', 'Kazlauskas', '2003-02-27', 'tgilvonas@gmail.com', 'Lorem ipsum dolor sit amet.', '2018-09-22 22:35:50'),
(10, 'Jonas', 'Petrauskas', '2001-01-24', '', 'Lorem ipsum blah blah.', '2018-09-23 14:08:04'),
(11, 'John', 'Doe', '2001-01-24', '', 'I say blah blah.', '2018-09-23 14:09:34'),
(12, 'Jane', 'Smith', '2002-04-08', 'jane.smith@example.com', 'Lorem ipsum', '2018-09-23 14:33:44'),
(13, 'Petras', 'Kavaliauskas', '2001-01-24', 'petras@example.com', 'Lorem ipsum dolor sit amet, blah blah.', '2018-09-23 14:35:07'),
(14, 'Peter', 'Johnson', '2003-05-08', '', 'Lorem ipsum', '2018-09-23 14:41:41'),
(15, 'John', 'Peterson', '2001-06-05', 'john@example.com', 'I love this message board.', '2018-09-23 14:47:58'),
(16, 'Steve', 'Peterson', '2001-01-24', '', 'Lorem ipsum dolor sit amet.', '2018-09-23 14:51:35'),
(17, 'Petras', 'Petrauskas', '2001-01-24', '', 'Lorem ipsum, blah blah.', '2018-09-23 14:53:37'),
(18, 'Ann', 'Smith', '2001-01-24', 'ann@example.com', 'I love you all.', '2018-09-23 15:31:11'),
(19, 'Steve', 'Peterson', '2002-05-27', '', 'Lorem ipsum dolor sit amet.', '2018-09-23 15:35:21'),
(20, 'Petras', 'Petrauskas', '2001-01-24', 'petras@example.com', 'Hey all. Hello.', '2018-09-23 15:49:37'),
(21, 'Jonas', 'Jonauskas', '2002-02-27', '', 'Lorem ipsum dolor sit amet.', '2018-09-23 15:53:25'),
(22, 'Dainius', 'Vilkauskas', '2001-01-24', 'dainius@example.com', 'Lorem ipsum dolor sit amet.', '2018-09-23 15:56:57'),
(23, 'Ala', 'Shriubovskaya', '2001-01-24', '', 'Privet vsem.', '2018-09-23 16:09:27'),
(24, 'Juozas', 'Juozauskas', '2002-05-08', 'juozas@example.com', 'Lorem ipsum, blah blah.', '2018-09-23 16:12:58'),
(25, 'Steve', 'Peterson', '2003-05-08', '', 'Hello World.', '2018-09-23 16:16:10'),
(26, 'Petras', 'Petrauskas', '2001-01-24', 'petras@example.com', 'Lorem ipsum dolor sit amet, blah blah.', '2018-09-23 16:28:16');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
