To install the application:
1. Create database, import file db_structure_and_data.sql
2. Edit file /config/config.php :
2.1. Define constant BASE_URL which determines base URL of your app.
2.2. Define constant MESSAGES_PER_PAGE which determines how many messages per page should be shown;
2.3. Set database connection settings
