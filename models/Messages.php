<?php

class Messages extends Model {
    
    protected $db;
    
    /**
     * Get a portion of messages by current page
     * @param int $page
     * @return array
     */
    public function getPageOfMessages($page=null)
    {
        // determining current page:
        if (!is_numeric($page)) {
            $page = 1;
        }
        
        // calculating offset:
        $offset = ($page-1) * MESSAGES_PER_PAGE;
        
        // setting variable $messages_per_page for bindParam function
        $messages_per_page = MESSAGES_PER_PAGE;
        
        $statement = $this->db->prepare('SELECT *, (floor(datediff(created_at, birthdate)/365)) AS `age` FROM `messages` ORDER BY `created_at` DESC LIMIT :limit OFFSET :offset');
        $statement->bindParam(':limit', $messages_per_page, PDO::PARAM_INT);
        $statement->bindParam(':offset', $offset, PDO::PARAM_INT);
        $statement->execute();
        
        return $statement->fetchAll();
    }
    
    /**
     * Get data for paginator (total pages, links to next and previous pages, etc.)
     * @param int $page
     * @return array
     */
    public function getPaginatorData($page=null)
    {
        // calculating how many messages are in database:
        $result = $this->db->prepare("SELECT COUNT(*) FROM `messages`"); 
        $result->execute(); 
        $rowsTotal = $result->fetchColumn();
        
        // determining page:
        if (!is_numeric($page)) {
            $page = 1;
        }
        
        // calculating pages total:
        $pagesTotal = ceil($rowsTotal / MESSAGES_PER_PAGE);
        
        // setting "previous page" URL:
        if ($page>1) {
            $prevPage = $page - 1;
            $prevPageUrl = '?page='.$prevPage;
        } else {
            $prevPageUrl = '#';
        }
        
        // setting "next page" URL:
        if ($page<$pagesTotal) {
            $nextPage = $page + 1;
            $nextPageUrl = '?page='.$nextPage;
        } else {
            $nextPageUrl = '#';
        }
        
        return [
            'current_page'=>$page,
            'previous_page_url'=>$prevPageUrl,
            'next_page_url'=>$nextPageUrl,
            'pages_total'=>$pagesTotal
        ];
    }
    
    /**
     * Insert new message into database
     * @param string $name
     * @param string $lastname
     * @param string $birthdate
     * @param string $email
     * @param string $message
     */
    public function insertMessage($name, $lastname, $birthdate, $email, $message)
    {
        $statement = $this->db->prepare("INSERT INTO `messages` (name, lastname, birthdate, email, message, created_at)
            VALUES(:name, :lastname, :birthdate, :email, :message, :created_at)");
        $statement->execute([
            "name" => strip_tags($name),
            "lastname" => strip_tags($lastname),
            "birthdate" => strip_tags($birthdate),
            "email" => strip_tags($email),
            "message" => strip_tags($message),
            "created_at" => date('Y-m-d H:i:s')
        ]);
    }
    
    /**
     * Validate message
     * @param string $name
     * @param string $lastname
     * @param string $birthdate
     * @param string $email
     * @param string $message
     * @return array
     */
    public function validateMessage($name, $lastname, $birthdate, $email, $message)
    {
        $error = false;
        $errors = [];
        $previousValues = [];
        
        if (empty($name) || !ctype_alpha($name)) {
            $error = true;
            $errors['name'] = 'Jūs privalote įvesti savo vardą';
        }
        if (empty($lastname) || !ctype_alpha($lastname)) {
            $error = true;
            $errors['lastname'] = 'Jūs privalote įvesti savo pavardę';
        }
        if (!$this->validateDate($birthdate)) {
            $error = true;
            $errors['birthdate'] = 'Jūs privalote įvesti savo gimimo datą MMMM-mm-dd formatu';
        }
        if (!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $error = true;
            $errors['email'] = 'Jūs privalote įvesti savo el pašto adresą';
        }
        if (empty(strip_tags($message))) {
            $error = true;
            $errors['message'] = 'Jūs privalote įvesti žinutės tekstą';
        }
        
        if ($error) {
            $previousValues = compact('name', 'lastname', 'birthdate', 'email', 'message');
        }
        
        return compact('error', 'errors', 'previousValues');
    }
    
    /**
     * Validate date
     * @param string $date
     * @param string $format
     * @return bool
     */
    public function validateDate($date, $format='Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }
    
    /**
     * For debugging PDO statements
     * @param string $query
     * @param string $params
     * @return string
     */
    public static function interpolateQuery($query, $params) {
        $keys = array();

        // build a regular expression for each parameter
        foreach ($params as $key => $value) {
            if (is_string($key)) {
                $keys[] = '/:'.$key.'/';
            } else {
                $keys[] = '/[?]/';
            }
        }

        $query = preg_replace($keys, $params, $query, 1, $count);

        return $query;
    }
    
}
