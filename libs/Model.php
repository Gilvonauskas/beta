<?php

class Model {
    
    protected $db;
    
    public function __construct()
    {
        // $db should be inherited by all models
        $this->db = DB::getInstance()->getConnection();
    }
    
}
