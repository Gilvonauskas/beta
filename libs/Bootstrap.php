<?php

class Bootstrap {
    
    private $controllers_directory = 'controllers';
    private $models_directory = 'models';
    private $libs_directory = 'libs';
    
	public function __construct() {
        
        session_start();
        
        $this->loadLibs();
        $this->loadModels();
        
        // routing implemented in simple way: /controller/action
        
		$url = isset($_GET['url']) ? $_GET['url'] : null;
		$url = rtrim($url, '/');
		$url = explode('/', $url);
        
        if (!empty($url[0])) {
            $url[0] = str_replace('.php', '', $url[0]);
        } else {
            // if url segment "controller" is empty:
			require_once $this->controllers_directory.'/index.php';
			$controller = new Index();
			$controller->index();
			return false;
		}
        
        // if url segment "controller" not empty:
		$file = $this->controllers_directory.'/' . $url[0] . '.php';
		if (file_exists($file)) {
			require_once $file;
		} else {
			return $this->notFound();
		}
		
		$controller = new $url[0];
        
        // if url segment "action" not empty:
        if (isset($url[1])) {
            if (method_exists($controller, $url[1])) {
                $controller->{$url[1]}();
            } else {
                return $this->notFound();
            }
        } else {
            $controller->index();
        }
        
        DB::getInstance()->disconnect();
	}
	
	private function notFound()
    {
		require_once $this->controllers_directory.'/exceptions.php';
		$controller = new ExceptionsController();
		$controller->notFound();
        DB::getInstance()->disconnect();
		return false;
	}
    
    private function loadLibs()
    {
        if ($libs_directory = opendir($this->libs_directory)) {
            while ( false !== ($file = readdir($libs_directory))) {
                if ($file!='.' && $file!='..') {
                    require_once $this->libs_directory.'/'.$file;
                }
            }
            closedir($libs_directory);
        }
    }
    
    private function loadModels()
    {
        if ($models_directory = opendir($this->models_directory)) {
            while ( false !== ($file = readdir($models_directory))) {
                if ($file!='.' && $file!='..') {
                    require_once $this->models_directory.'/'.$file;
                }
            }
            closedir($models_directory);
        }
    }
    
}