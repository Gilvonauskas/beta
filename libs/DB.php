<?php

class DB {

    private static $instance = null;
    private $connection;
    
    private function __construct()
    {
        try {
            $this->connection = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASSWORD,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
        } catch (PDOException $ex) {
            print "Error!: " . $ex->getMessage() . "<br/>";
            die();
        }
    }

    public static function getInstance()
    {
        if(!self::$instance) {
            self::$instance = new DB();
        }
        return self::$instance;
    }

    public function getConnection()
    {
        return $this->connection;
    }
    
    public function disconnect()
    {
        $this->connection = null;
    }
    
}
