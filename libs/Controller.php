<?php

class Controller {
    
    public $view;
    public $flashData;
    
	public function __construct() {
        // setting "flash data" which has lifetime of only one request:
        if (isset($_SESSION['flash_data'])) {
            $this->flashData = $_SESSION['flash_data'];
            $_SESSION['flash_data'] = null;
        }
        
        // setting CSRF token
        if (!isset($_SESSION['csrf_token'])) {
            $_SESSION['csrf_token'] = hash('sha256', rand(1000, 9999));
        }
        
        // initializing view object:
		$this->view = new View();
	}
    
    public function get_csrf_token()
    {
        return $_SESSION['csrf_token'];
    }
    
}