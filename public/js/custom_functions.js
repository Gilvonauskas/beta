$(document).ready(function(){
    
    $('#message_form').submit(function(event){
        event.preventDefault();
        
        var formValid = validateForm();
        
        if (formValid) {
            
            // getting current page from $_GET params:
            var url_string = window.location.href;
            var url = new URL(url_string);
            var page = url.searchParams.get("page");
            
            // serializing form data: 
            var formData = $('#message_form').serializeArray();
            // adding page param:
            formData.push({name: 'page', value: page});
            
            // "loading": disabling inputs, showing loader:
            $('#message_form input[type="text"], #message_form textarea').attr('disabled', 'disabled');
            $('#submit').hide();
            $('#loader').show();
            
            $.ajax({
                method: 'POST',
                url: '/index/post_message_ajax',
                data: formData,
                success: function(responseData){
                    
                    // "loaded": enabling inputs, hiding loader:
                    $('#message_form input[type="text"], #message_form textarea').val('').removeAttr('disabled');
                    $('#submit').show();
                    $('#loader').hide();
                    
                    // clearing messages list element:
                    $('.messages-list').html('');
                    
                    // iterate throug messages from AJAX response:
                    $.each(responseData.messages, function(index, message){
                        
                        // ".message-to-copy li" is hidden the element where we set values, clone and append to .messages-list
                        $('.message-to-copy li .created_at').text(message.created_at);
                        
                        if (message.email.length>0) {
                            $('.message-to-copy li .name_lastname_without_email').hide();
                            $('.message-to-copy li .name_lastname_with_email').show();
                        } else {
                            $('.message-to-copy li .name_lastname_with_email').hide();
                            $('.message-to-copy li .name_lastname_without_email').show();
                        }
                        
                        $('.message-to-copy li .name_lastname_with_email a').attr('href', 'mailto:'+message.email);
                        $('.message-to-copy li .name_lastname').text(message.name+' '+message.lastname);
                        $('.message-to-copy li .age').text(message.age);
                        
                        $('.message-to-copy li .message').text(message.message);
                        
                        $('.message-to-copy li').clone().appendTo('.messages-list');
                    });
                    
                    // clearing paginator element:
                    $('#pages').html('');
                    
                    // setting elements of paginator from AJAX response:
                    // .pages-to-copy is hidden element where inner elements are taken, set and appended to #pages element:
                    $('.pages-to-copy .prev_page').attr('href', responseData.paginator.previous_page_url).clone().appendTo('#pages');
                    for (i=1; i<=responseData.paginator.pages_total; i++) {
                        if (i==responseData.paginator.current_page) {
                            $('.pages-to-copy .current_page').text(i).clone().appendTo('#pages');
                        } else {
                            $('.pages-to-copy .page_link').attr('href', '?page='+i).text(i).clone().appendTo('#pages');
                        }
                    }
                    $('.pages-to-copy .next_page').attr('href', responseData.paginator.next_page_url).clone().appendTo('#pages');
                    
                }
            });
        }
    });
});

function validateForm() {
    var formValid = true;
        
    if (/^[a-z]+$/i.test($('#name').val())==false) {
        formValid = false;
        $('#name').parents('p').addClass('err');
    } else {
        $('#name').parents('p').removeClass('err');
    }
    
    if (/^[a-z]+$/i.test($('#lastname').val())==false) {
        formValid = false;
        $('#lastname').parents('p').addClass('err');
    } else {
        $('#lastname').parents('p').removeClass('err');
    }
    
    if ( !dateIsValid($('#birthdate').val()) ) {
        formValid = false;
        $('#birthdate').parents('p').addClass('err');
    } else {
        $('#birthdate').parents('p').removeClass('err');
    }
    
    if ( $('#email').val()!='' && !validateEmail($('#email').val()) ) {
        formValid = false;
        $('#email').parents('p').addClass('err');
    } else {
        $('#email').parents('p').removeClass('err');
    }
    
    if ($('#message').val()=='') {
        formValid = false;
        $('#message').parents('p').addClass('err');
    } else {
        $('#message').parents('p').removeClass('err');
    }
    
    return formValid;
}

function dateIsValid(date) {
    var timestamp = Date.parse(date);
    return !isNaN(timestamp);
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
