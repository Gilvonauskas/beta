<?php

class ExceptionsController extends Controller {

	public function __construct() {
		parent::__construct();
	}
    
    public function notFound()
    {
        $this->view->render('exceptions/not_found');
    }
    
}