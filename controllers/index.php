<?php

/*
 * License: free to use.
 *
 * @author Tomas Gilvonauskas
 */
class index extends Controller {
    
    private $messagesModel;
    
    // variable which stores data just for one request:
    public $flashData;
    
    public function __construct()
    {
        parent::__construct();
        
        // initiate Messages model:
        $this->messagesModel = new Messages();
    }
    
    public function index()
    {
        // determine page of messages:
        if (!isset($_GET['page']) || !is_numeric($_GET['page'])) {
            $page = 1;
        } else {
            $page = $_GET['page'];
        }
        
        // get portion of messages by page and pass the data of messages to the view:
        $this->view->messages = $this->messagesModel->getPageOfMessages($page);
        
        // get data about paginator and pass it to the view:
        $this->view->paginator = $this->messagesModel->getPaginatorData($page);
        
        // set flash data
        $this->view->flashData = $this->flashData;
        
        // render view:
        $this->view->render('index/index');
    }
    
    public function post_message()
    {
        // if POSTed CSRF token does not match token in session:
        if ($_POST['csrf_token'] != $this->get_csrf_token()) {
            // question: how to react if csrf token does not match?
            exit;
        }
        
        // validate POSTed message and get validation result as array:
        $validationResult = $this->messagesModel
                ->validateMessage($_POST['name'], $_POST['lastname'], $_POST['birthdate'], $_POST['email'], $_POST['message']);
        
        // if POSTed message is not valid:
        if ($validationResult['error']) {
            $_SESSION['flash_data'] = $validationResult;
            header('Location: '.BASE_URL);
        }
        
        // insert message into database:
        $this->messagesModel
                ->insertMessage($_POST['name'], $_POST['lastname'], $_POST['birthdate'], $_POST['email'], $_POST['message']);
        
        // redirect:
        header('Location: '.BASE_URL);
    }
    
    public function post_message_ajax()
    {
        // if POSTed CSRF token does not match token in session:
        if ($_POST['csrf_token'] != $this->get_csrf_token()) {
            // question: how to react if csrf token does not match?
            exit;
        }
        
        // insert message into database:
        $this->messagesModel
                ->insertMessage($_POST['name'], $_POST['lastname'], $_POST['birthdate'], $_POST['email'], $_POST['message']);
        
        // determine current page
        if (!isset($_GET['page']) || !is_numeric($_GET['page'])) {
            $page = 1;
        } else {
            $page = $_GET['page'];
        }
        
        // get messages of current page:
        $messages = $this->messagesModel->getPageOfMessages($page);
        
        // get data for paginator of current page:
        $paginator = $this->messagesModel->getPaginatorData($page);
        
        // output data in JSON:
        header('Content-Type: application/json');
        echo json_encode(compact('messages', 'paginator'));
    }
    
}
