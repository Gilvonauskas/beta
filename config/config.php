<?php

// base URL of app:
define('BASE_URL', 'http://beta.local');

// libraries directory:
define('LIBS', 'libs/');

// constant which determines how many messages per page to show:
define('MESSAGES_PER_PAGE', 5);

// database connection settings:
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'beta');
